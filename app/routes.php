<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route::get('/', function()
// {
// 	return Redirect::to('/usuarios');
// });

Route::get('/usuarios', array('uses' => 'UsuariosController@mostrarUsuarios'));
Route::get('/usuarios/nuevo', ['uses' => 'UsuariosController@nuevoUsuario']);
Route::post('/usuarios/crear',['uses' => 'UsuariosController@crearUsuario']);
Route::get('/usuarios/{id}',['uses'=>'UsuariosController@verUsuario']);

Route::get('/carros', ['uses' => 'CarrosController@mostrarCarros']);
Route::post('/carros/crear', ['uses' => 'CarrosController@crearCarro']);
Route::post('/carros/{id}', ['uses' => 'CarrosController@buscarCarro']);

Route::get('/queries', ['uses' => 'CarrosController@queriesView']);
Route::get('/queries/select', ['uses' => 'CarrosController@queriesSelect']);
Route::get('/queries/insert', ['uses' => 'CarrosController@queriesInsert']);
Route::get('/queries/update', ['uses' => 'CarrosController@queriesUpdate']);
Route::get('/queries/delete', ['uses' => 'CarrosController@queriesDelete']);
Route::get('/queries/get', ['uses' => 'CarrosController@queriesGet']);
Route::get('/queries/get-with-model', ['uses' => 'CarrosController@queriesGetWithModel']);

Route::get('/validate-form', function(){
	return View::make('validaciones.formulario');
});
Route::post('/validate-form/validar', function(){
	$rules = [
							'nombre' => 'required',
							'usuario' => ['required', 'min:8'],
							'correo' => ['required', 'email'],
							'edad' => ['integer', 'min:18'],
							'sexo' => 'in:m,M,f,F'
						];

	$v = Validator::make(Input::all(), $rules);

	if ($v->fails()) {
		return Redirect::to('/validate-form')->withErrors($v)->withInput();
	} else {
		echo "Datos validos";
		exit;
	}
});

Route::get('/', function(){
	$vendedores = Vendedor::with('productos')->get();
	return View::make('proyecto.inicio', ['vendedores' => $vendedores]);
});
Route::get('/vendedor', ['uses' => 'VendedorController@showVendedores']);
Route::post('/vendedor', ['uses' => 'VendedorController@createVendedor']);

Route::get('/producto', ['uses' => 'ProductoController@showProductos']);
Route::post('/producto', ['uses' => 'ProductoController@createProducto']);

//Filter examples
Route::get('/', ['before' => 'filtro_antes:1,2,3']);

Group::get(['before' => 'nombre_filtro'], function(){
	Route::get('/', function(){
		//Code
	});
	Route::get('otraruta',['uses' => 'controlador']);
});
Route::when('administrador/*', 'filtro_aplicar');
