<?php
class CarrosTableSeeder extends Seeder {

    public function run()
    {
        DB::table('carros')->delete();

        Carro::create(array('model' => 'Model a', 'plate' => '1234ab', 'color' => 'Grey', 'year' => '2000'));
        Carro::create(array('model' => 'Model b', 'plate' => '5678cd', 'color' => 'Red', 'year' => '1990'));
        Carro::create(array('model' => 'Model c', 'plate' => '9101ef', 'color' => 'Blue', 'year' => '2014'));
        Carro::create(array('model' => 'Model d', 'plate' => '1213gh', 'color' => 'Green', 'year' => '2005'));
        Carro::create(array('model' => 'Model e', 'plate' => '1415ij', 'color' => 'yellow', 'year' => '2010'));
        Carro::create(array('model' => 'Model f', 'plate' => '1617kl', 'color' => 'yellow', 'year' => '2012'));
    }

}
