<?php
class VendedorTableSeeder extends Seeder {

    public function run()
    {
        DB::table('vendedor')->delete();

        Vendedor::create(array('nombre' => 'Vendedor', 'apellido' => 'a'));
        Vendedor::create(array('nombre' => 'Vendedor', 'apellido' => 'b'));
        Vendedor::create(array('nombre' => 'Vendedor', 'apellido' => 'c'));
        Vendedor::create(array('nombre' => 'Vendedor', 'apellido' => 'd'));
        Vendedor::create(array('nombre' => 'Vendedor', 'apellido' => 'e'));
        Vendedor::create(array('nombre' => 'Vendedor', 'apellido' => 'f'));
    }

}
