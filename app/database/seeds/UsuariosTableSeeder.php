<?php

class UsuariosTableSeeder extends Seeder {

    public function run()
    {
        DB::table('usuarios')->delete();

        Usuario::create(array('nombre' => 'Ayoze', 'apellido' => 'Vera'));
        Usuario::create(array('nombre' => 'Jose', 'apellido' => 'Ruano'));
        Usuario::create(array('nombre' => 'Esperanza', 'apellido' => 'Gutierrez'));
        Usuario::create(array('nombre' => 'Casimiro', 'apellido' => 'Miranda'));
    }

}
