<?php
class ProductoTableSeeder extends Seeder {

    public function run()
    {
        DB::table('producto')->delete();
        $i = Vendedor::first();
        $i = $i['id'];
        Producto::create(array('vendedor_id' => $i, 'descripcion' => 'Producto A', 'precio' => '10'));
        Producto::create(array('vendedor_id' => $i, 'descripcion' => 'Producto B', 'precio' => '20'));
        $i++;
        Producto::create(array('vendedor_id' => $i, 'descripcion' => 'Producto C', 'precio' => '15'));
        Producto::create(array('vendedor_id' => $i, 'descripcion' => 'Producto D', 'precio' => '12'));
        Producto::create(array('vendedor_id' => $i, 'descripcion' => 'Producto E', 'precio' => '25'));
        Producto::create(array('vendedor_id' => $i, 'descripcion' => 'Producto F', 'precio' => '69'));
    }

}
