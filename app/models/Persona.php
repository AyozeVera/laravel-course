<?php

class Persona extends Eloquent { //All models must extend Eloquent

  public function pasaporte()
  {
    return $this->hasOne('Pasaporte', 'persona_id');
    // Here we are saying that the relationship between Pasaporte and Persona.
    // 2 parameters: First is the model to make the relationship and the other is
    // how to make the relationship
    // Elocuent will check the Persona->id is equal to Pasaporte->persona_id
  }

  //Uso:
  $persona = Persona::find(1);
  $pasaporte = $persona->pasaporte;

}
