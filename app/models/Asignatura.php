<?php

class Asignatura extends Eloquent { //All models must extend Eloquent

  public function temas()
  {
    return $this->hasMany('Tema', 'asignatura_id')
    // One to many relationship
  }

  public function profesores()
  {
    return $this->belongsToMany('Profesor', 'profesor_asignatura', 'asignatura_id', 'profesor_id');
  }
  // hasMany use ###############################
  $asignatura = Asignatura::find(1);
  $temas = $asignatura->temas;

  $temas_obligatorios = $asignatura->temas()->where('obligatorio', '=', '1')->get();
  // Use example. You can also add filters
  // #############################################

  // belongsToMany use ###########################

  $asignaturas = Profesor::find(1)->asignaturas;

  // #############################################

}
