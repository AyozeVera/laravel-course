<?php

class Tema extends Eloquent { //All models must extend Eloquent

  public function asignatura()
  {
    return $this->belongsTo('Asignatura', 'asignatura_id');
  }

  $asignatura = Tema::find(1)->asignatura;
  // returns the registry which theme 1 belongs to
}
