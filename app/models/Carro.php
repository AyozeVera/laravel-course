<?php

class Carro extends Eloquent { //All models must extend Eloquent

  protected $table = 'carros';

  protected $fillable = ['model','plate', 'color', 'year'];

}
