<?php

class Profesor extends Eloquent { //All models must extend Eloquent

  public function asignaturas()
  {
      // spanish: Muchos a muchos
      return $this->belongsToMany('Asignatura', 'profesor_asignatura', 'profesor_id', 'asignatura_id');
  }

  public function asignaturas(){
       return $this->belongsToMany('Asignatura', 'profesor_asignatura', 'profesor_id', 'asignatura_id')
                       ->withPivot('clase', 'hora');
    // withPivot allows us to we can add parameters from the other table
   }


  // belongsToMany use ###########################

  $profesores = Asignatura::find(1)->profesores;

  $profesor = Profesor::find(1);
  $asignaturas = $profesor->asignaturas;

  foreach ($asignaturas as $asignatura) {
    echo $asignatura->nombre;
    //if we want to use parameters from the intermediate table we must use pivot
    echo $asignatura->pivot->salon;
  }

  // #############################################

  $profesor = Profesor::find(1);
  $profesor->asignaturas()->attach(5);
  // We add the asignatura with id=5 to $profesor
  $profesor->asignaturas()->attach(5, ['salon'=>'14', 'hora'=>'5:00 PM']);
  // Using intermediate table
  $profesor->asignaturas()->detach(5);
  // Remove asignation

  // #############################################
}
