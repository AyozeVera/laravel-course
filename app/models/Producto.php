<?php
class Producto extends Eloquent {

  protected $table = 'producto';
  protected $fillable = ['descripcion', 'precio', 'vendedor_id'];

  public static function addProducto($input)
  {
    $res = [];

    $rules = [
                'vendedor_id' => ['required'],
                'descripcion' => ['required', 'max:250'],
                'precio' => ['required', 'numeric']
              ];

  $v = Validator::make($input, $rules);

  if ($v->fails()) {
    $res['message'] = $v;
    $res['error'] = true;
  } else {
    $producto = static::create($input);
    $res['message'] = 'Producto creado correctamente';
    $res['error'] = false;
    $res['data'] = $producto;
  }

    return $res;
  }

}
