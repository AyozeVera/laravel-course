<?php

class Vendedor extends Eloquent {

  protected $table = 'vendedor';
  protected $fillable = ['nombre', 'apellido'];

  public function productos()
  {
    return $this->hasMany('Producto', 'vendedor_id');
  }

  public static function addVendedor($input)
  {
    $res = [];
    $rules = [
                'nombre' => ['required', 'max:100'],
                'apellido' => ['required', 'max:100'],
              ];
    $v = Validator::make($input,$rules);
    if ($v->fails()) {
      $res['message'] = $v;
      $res['error'] = true;
    } else {
      $vendedor = static::create($input);
      $res['message'] = 'Vendedor Creado';
      $res['error'] = false;
      $res['data'] = $vendedor;
    }

  return $res;
  }

}
