<?php

class Articulo extends Eloquent {

  public function comentarios()
  {
    return $this->hasMany('Comentario');
  }

}
