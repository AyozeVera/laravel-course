@extends('layouts.master')

@section('sidebar')
  @parent
  Formulario de Usuarios
@stop

@section('content')
  {{HTML::link('usuarios', 'volver');}}
  <h1>Crear Usuarios</h1>
  {{Form::open(['url'=>'usuarios/crear'])}}
    {{Form::label('nombre', 'Nombre')}}
    {{Form::text('nombre', '')}}
    {{Form::label('apellido', 'Apellido')}}
    {{Form::text('apellido', '')}}
    {{Form::submit('Guardar')}}
  {{Form::close()}}
@stop
