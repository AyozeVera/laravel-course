
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Validaciones</title>
</head>
<body>
  {{ Form::open(array('url' => 'validate-form/validar')) }}
  <h1>Formulario</h1>

  <div>
    {{ Form::label('nombre', 'Nombre: ')}}<br />
    {{ Form::text('nombre', '')}}
    @if( $errors->has('nombre') )
             @foreach($errors->get('nombre') as $error )
                 <br />* {{ $error }}
             @endforeach
         @endif
  </div>

  <div>
    {{ Form::label('usuario', 'Usuario: ')}}<br />
    {{ Form::text('usuario', '')}}
    @if( $errors->has('usuario') )
             @foreach($errors->get('usuario') as $error )
                 <br />* {{ $error }}
             @endforeach
         @endif
  </div>

  <div>
    {{ Form::label('correo', 'Correo: ')}}<br />
    {{ Form::text('correo', '')}}
    @if( $errors->has('correo') )
             @foreach($errors->get('correo') as $error )
                 <br />* {{ $error }}
             @endforeach
         @endif
  </div>

  <div>
    {{ Form::label('edad', 'Edad: ')}}<br />
    {{ Form::text('edad', '' )}}
    @if( $errors->has('edad') )
             @foreach($errors->get('edad') as $error )
                 <br />* {{ $error }}
             @endforeach
         @endif
  </div>

  <div>
    {{ Form::label('sexo', 'Sexo: ')}}<br />
    {{ Form::text('sexo', '')}}
    @if( $errors->has('sexo') )
             @foreach($errors->get('sexo') as $error )
                 <br />* {{ $error }}
             @endforeach
         @endif
  </div>

  <div>
    {{ Form::submit('Enviar')}}
  </div>

  {{ Form::close()}}
</body>
</html>
