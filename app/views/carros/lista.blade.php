@extends('layouts.master')

@section('sidebar')
  @parent
  Lista de Carros
@stop

@section('content')
  <h1>Carros</h1>
  <ul>
  {{HTML::link('queries', 'See Queries');}}
  <table>
    <tr>
      <th>Id</th>
      <th>Modelo</th>
      <th>Matrícula</th>
      <th>Año</th>
    </tr>
    @foreach($carros as $carro)
      <tr>
        <td>{{$carro->id}}</td>
        <td>{{$carro->model}}</td>
        <td>{{$carro->plate}}</td>
        <td>{{$carro->year}}</td>
      </tr>
    @endforeach
  </table>
  <div class="" style="margin-top: 30px;">
    {{Form::open(['url'=>'carros/crear'])}}
      {{Form::label('model')}}
      {{Form::text('model', '')}}
      {{Form::label('plate')}}
      {{Form::text('plate', '')}}
      {{Form::label('year')}}
      {{Form::text('year', '')}}
      {{Form::submit('Guardar')}}
    {{Form::close()}}
  </div>
  <div class="" style="margin-top: 30px;">
    {{Form::open(['url'=>'carros/{id}'])}}
      {{Form::label('ID')}}
      {{Form::text('id', '')}}
      {{Form::submit('Buscar')}}
    {{Form::close()}}
  </div>

@stop
