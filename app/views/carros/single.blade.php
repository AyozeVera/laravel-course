@extends('layouts.master')

@section('sidebar')
  @parent
  Lista de Carros
@stop

@section('content')
  <h1>Carro {{$carro->id}}</h1>
  <p>
    {{$carro->model . ' - ' . $carro->plate . ' - ' . $carro->year;}}
  </p>
  <p>
    {{HTML::link('carros', 'Volver');}}
  </p>
@stop
