@extends('layouts.master')

@section('sidebar')
  @parent
  Queries
@stop

@section('content')
<ul>
    <li>{{HTML::link('queries/select', 'Query select');}}</li>
    <li>{{HTML::link('queries/insert', 'Query insert[timestamps failing]');}}</li>
    <li>{{HTML::link('queries/update', 'Query update');}}</li>
    <li>{{HTML::link('queries/delete', 'Query delete');}}</li>
    <li>{{HTML::link('queries/get', 'Query get');}}</li>
    <li>{{HTML::link('queries/get-with-model', 'Query get using Carros model');}}</li>
</ul>
@stop
