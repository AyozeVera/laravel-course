<?php
class ProductoController extends BaseController {

  public function showProductos()
  {
    $productos = Producto::all();
    $vendedores = Vendedor::all();
    return View::make('producto.lista', ['productos' => $productos, 'vendedores' => $vendedores]);
  }

  public function createProducto()
  {
    $res = Producto::addProducto(Input::all());

    if ($res['error'] == true) {
      return Redirect::to('producto')->withErrors($res['message'])->withInput();
    } else {
      return Redirect::to('producto')->with('message', $res['message']);
    }
  }

}
