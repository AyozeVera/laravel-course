<?php
class HelpersController extends BaseController {

  function add_array_example()
  {
    $array = ['nombre' => 'Ayo'];
    $array = array_add($array, 'apellido', 'Vera');
    //add key: apellido with value 'Vera' to $array
  }

  function array_except_example()
  {
    $array = ['key'=>'will be deleted',
              'first'=>'wont delete',
              'second'=>'wont delete',
              'delete'=>'will be deleted'];
    $delete = ['key, delete'];

    $array = array_except($array, $delete);

    //delete the keys in $delete
  }

  function paths_example()
  {
    var_dump(app_path()); // /app dir route
    var_dump(base_path()); //proyect dir route
    var_dump(public_path()); // /public dir route
    var_dump(storage_path()); // /storage dir route
  }

  function camel_case_example()
  {
    $camel =camel_case('hello_world');
    // $camel = 'helloWorld'

  }

  function snake_case_example()
  {
    $snake =snake_case('helloWorld');
    // $camel = 'hello_world'

  }

  function e_example()
  {
    $text = e('helloWorld');
    // return the parameter as text not as html
  }

  function ends_with_example()
  {
    $text = ends_with('hello World', 'World');
    // return true
  }

  function starts_with_example()
  {
    $text = starts_with('hello World', 'hello');
    // return true
  }

  function str_contains_example()
  {
    $text = str_contains('hello World of course', 'of');
    // return true
  }

  function str_is_example()
  {
    $text = str_is('of', 'hello World of course');
    // return false, couse look for exactly of
    $text = str_is('*of', 'hello World of course');
    // return true
    $text = str_is('*world*of', 'hello World of course');
    // return true
  }

  function str_random_example()
  {
    $text = str_random(15);
    // return a 15 random characters string
  }

  function dd_example()
  {
    $name = 'pepe';
    dd('$name');
    //var_dump($name) + die();
  }


}
