<?php
class UsuariosController extends BaseController {

  /**
  * Show all Usuarios list
  */
  public function mostrarUsuarios()
  {
    $usuarios = Usuario::all();
    return View::make('usuarios.lista', array('usuarios'=>$usuarios));
  }

  /**
  * Show create user form
  */
  public function nuevoUsuario()
  {
    return View::make('usuarios.crear');
  }

  /**
  * Create user on DB
  */
  public function crearUsuario()
  {
    //create method creates an user on the DB
    //with Input::all() we put the form content
    //on the DB
    $form = Input::all();
    Usuario::create([
        'nombre' => $form['nombre'],
        'apellido' => $form['apellido']
    ]);
    //Redirect redirect us back to usuarios view
    return Redirect::to('/usuarios');
  }

  /**
  * See a concret user
  */
  public function verUsuario($id)
  {
    //find user with id=$id in the DB
    $usuario = Usuario::find($id);
    //create view usuario.ver
    return View::make('usuarios.ver', ['usuario' => $usuario]);
  }

}
