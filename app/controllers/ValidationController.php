<?php

class ValidationController extends BaseController{

  $data = ['Nombre' => 'Pedro'];
  $rules1 = ['Nombre', 'required'];
  $rules2 = ['Nombre', 'min:5'];
  $rules3 = ['Nombre' => ['required', 'min:5']];
  $rules4 = [
              'Nombre' => ['required', 'min:5'],
              'Edad' => 'integer'
            ];

  // ### Some rules examples ##############################
  $rules5 = 'campo' => ['after:10/12/2015', 'before:12/12/2015'];
  $rules6 = 'campo' => 'between:5,7';
  $rules7 = 'campo' => 'date_format:d/m/y';
  $rules8 = 'campo' => 'mimes:pdf,doc,docx'; //format tipe verification
  $rules9 = 'campo' => 'regex:[a-z]'; //parameter must comply the regular expression
  $rules10 = 'campo' => 'same:campo1'; // campo must be equal to campo1
  $rules11 = 'campo' => 'size:7';
  $rules12 = 'campo' => 'exists:tabla,campo_tabla'; // must exists a table tabla and an paramter campo_taba in table tabla

  // ######################################################

  $v = Validator::make($datos, $rules4);

  if ($messages->has('edad')) {
    foreach ($messages->get('edad') as $message) {
      echo $message . '<br>';
    }
  }

  if ($v->fails()) {
    $messages = $v->messages();
    foreach ($messages->all() as $message) {
      echo $message . '<br>';
    }
  } else {
    echo 'Datos correctos';
  }
}
