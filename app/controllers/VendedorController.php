<?php
class VendedorController extends BaseController {

  public function showVendedores()
  {
    $vendedores = Vendedor::all();
    return View::make('vendedor.lista', ['vendedores' => $vendedores]);
  }

  public function createVendedor()
  {
    $res = Vendedor::addVendedor(Input::all());

    if ($res['error'] == true) {
      return Redirect::to('vendedor')->withErrors($res['message'])->withInput();
    } else {
      return Redirect::to('vendedor')->with('message', $res['message']);
    }
  }

}
