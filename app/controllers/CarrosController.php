<?php
class CarrosController extends BaseController {

  public function mostrarCarros()
  {
    $carros = $this->getCarros();
    return View::make('carros.lista',['carros' => $carros]);
  }

  public function crearCarro()
  {
    $form = Input::all();

    if (isset($form['model']) && isset($form['plate']) && isset($form['year'])) {
      Carro::create([
          'model' => $form['model'],
          'plate' => $form['plate'],
          'year' => $form['year']
      ]);
    }
    return Redirect::to('/carros');
  }

  public function buscarCarro($id)
  {
    $input = Input::all();
    $carro = $this->getCarro($input['id']);
    return View::make('carros.single',['carro' => $carro]);
  }

  private function getCarros()
  {
    return Carro::all();
  }

  private function getCarro($id)
  {
    return Carro::find($id);
  }
  // QUERIES

  public function queriesView()
  {
    return View::make('carros.query');
  }

  public function queriesSelect()
  {
    $resultado = DB::select('SELECT * FROM carros WHERE color = ?', ['yellow']);
    echo serialize($resultado);
  }
  public function queriesInsert()
  {
    $resultado = DB::insert('INSERT INTO carros (id, model, color, plate, year) VALUES (?, ?, ?, ?, ?)', ['69','QQ', 'Green', 'GTF666', '2011']);
    return View::make('carros');
  }
  public function queriesUpdate()
  {
    $resultado = DB::update('UPDATE carros SET model = ? WHERE id = ?', ['changed','7']);
    return Redirect::to('carros');
  }
  public function queriesDelete()
  {
    $resultado = DB::update('DELETE FROM  carros WHERE id = ?', ['8']);
    return Redirect::to('carros');
  }
  public function queriesGet()
  {
    $resultados = DB::table('carros')->where('id', '>=', '11')->get();
    echo serialize($resultados);
  }
  public function queriesGetWithModel()
  {
    $resultadoA = DB::table('carros')->where('color', '=', 'Green')->get(array('id', 'model', 'color'));
    $resultadoB = Carro::where('color', '=', 'Green')->get(array('id', 'model', 'color'));
    echo serialize($resultadoA) . '##' . $resultadoB;
  }
}
